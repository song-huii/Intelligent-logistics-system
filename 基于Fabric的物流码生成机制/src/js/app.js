App = {
    web3Provider: null,
    contracts: {},
    account: '0x0',
    init: function() {
        return App.initWeb3();
    },

    initWeb3: function() {
        // TODO: refactor conditional
        if (typeof web3 !== 'undefined') {
            // If a web3 instance is already provided by Meta Mask.
            App.web3Provider = web3.currentProvider;
            ethereum.enable();
            web3 = new Web3(web3.currentProvider);
        } else {
            // Specify default instance if no web3 instance provided
            App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
            ethereum.enable();
            web3 = new Web3(App.web3Provider);
        }
        return App.initContract();
    },




    initContract: function() {
        $.getJSON("Election.json", function(election) {
            // 初始化合约
            App.contracts.Election = TruffleContract(election);
            // 连接与合约进行交互
            App.contracts.Election.setProvider(App.web3Provider);

            App.listenForEvents();
            //  App.allCreated();
            App.capacity();
            return App.render();
        });
    },

    // 监听合约事件
    listenForEvents: function() {
        App.contracts.Election.deployed().then(function(instance) {



        });
    },






    render: function() {
        // Load account data
        web3.eth.getCoinbase(function(err, account) {
            if (err === null) {
                App.account = account;
                $("#accountAddress").html("当前账户地址: " + account);


                web3.eth.getBalance(account, function(err, res) {
                    if (!err) {
                        console.log(res);
                        $("#accBalance").html("当前账户余额: " + res + 'wei');
                    } else {
                        console.log(err);
                    }
                });


            }
        });




        // Load contract data
        App.contracts.Election.deployed().then(function(instance) {
            electionInstance = instance;
            return electionInstance._capacity();
        }).then(function(_capacity) {
            var Powernamea = $("#allproducts");
            Powernamea.empty();
            for (var i = 1; i <= _capacity; i++) {
                electionInstance.productnews(i).then(function(candidate) {
                    var id = candidate[0];
                    var nums = candidate[1];
                    var createTime = candidate[2];
                    var nowhold = candidate[3];
                    var createMan = candidate[4];
                    var names = candidate[5];
                    var unixTimestamp = new Date(createTime * 1000);
                    var createTime = unixTimestamp.toLocaleString()
                        // Render candidate Result
                    var candidateTemplate = "<tr><td>" + "产品序号：" + id + "</td></tr><tr><td>" + "产品名称：" +
                        names + "</td></tr></tr><tr><td>" + "创建区块：" + createTime + "</td></tr><tr><td>" + "当前持有者：" +
                        nowhold + "</td></tr><tr><td>" + "产品生产厂家: " + createMan + "</td></tr>   <tr><td>" + "产品溯源码：" +
                        nums + "</td></tr><tr><td> <img src='../images/" + 333 + ".jpg' height='100px' width='650px' /> </td></tr>"
                    var qID = document.cookie.split(";")[0].split("=")[1];
                    if (id == qID) {
                        Powernamea.append(candidateTemplate);

                    }

                });

            }

            //return electionInstance.voters(App.account);
        }).catch(function(error) {
            console.warn(error);
        });



    },



    quaryP: function() {
        qID = $('#qID').val();

        document.cookie = "qID=" + qID;

    },



    sendTransaction: function() {
        var fromAccount = $('#fromAccount').val();
        var toAccount = $('#toAccount').val();
        var amount = $('#amount').val();

        if (web3.isAddress(fromAccount) &&
            web3.isAddress(toAccount) &&
            amount != null && amount.length > 0
        ) {
            // Example 1: 使用Metamask 给的gas Limit 及 gas 价
            var message = { from: fromAccount, to: toAccount, value: web3.toWei(amount, 'ether') };


            web3.eth.sendTransaction(message, (err, res) => {
                var output = "";
                if (!err) {
                    output += res;
                } else {
                    output = "Error";
                }
                document.getElementById('transactionResponse').innerHTML = "Transaction response= " + output + "<br />";
            })
        } else {
            console.log("input error");
        }
    },



    capacity: function() {
        var adoptionInstance;
        var Powernames = $("#cargoNames");
        App.contracts.Election.deployed().then(function(instance) {
            adoptionInstance = instance;
            // 调用合约的getAdopters(), 用call读取信息不用消耗gas
            return adoptionInstance.capacity.call();
        }).then(function(_capacity) {

            var candidateTemplate = "<tr><td>" + "当前产品总数目：" + _capacity + "</td></tr>"
            Powernames.append(candidateTemplate);

            //  qID= $('#qID').val();

            document.cookie = "qID=" + _capacity;

        }).catch(function(err) {
            console.log(err.message);
        });
    },



    createProduct: function() {
        var productName = $('#productName').val();

        var userAccount = web3.eth.accounts[0];
        App.contracts.Election.deployed().then(function(instance) {
            return instance.createNewCargo(productName, { gas: 3000000, from: userAccount });

        }).then(function(cargoID) {


        }).catch(function(err) {
            console.error(err);
        });
    },



    transferProduct: function() {
        var productId = $('#productId').val();
        var toAdd = $('#toAdd').val();

        var userAccount = web3.eth.accounts[0];
        App.contracts.Election.deployed().then(function(instance) {
            return cargoID = instance.transfer(productId, toAdd, { gas: 3000000, from: userAccount });

        }).then(function(cargoID) {
            // Wait for to update

        }).catch(function(err) {
            console.error(err);
        });
    },








};

$(function() {
    $(window).load(function() {
        App.init();
    });
});