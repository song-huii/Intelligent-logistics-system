pragma solidity ^0.5.0;

// * 不是所有的用户都有权限创建一个产品，合约的唯一管理员账号可以设置用户的权限；
// * 任何人可以查询任何人的产品创建权限；
// * 有权限的用户可以创建一个产品并为其附一个名称，创建的产品会默认属于创建人，拥有可查验的创建者证明；
// * 产品的名称可重复，ID唯一，均不可修改；
// * 拥有某一产品的用户有权限将改产品转移给另一非零用户，产品的拥有者和转移权同时变更，而创建者身份不变，同时这次转移会记录；
// * 任何人可以查询特定产品的名称、交易转移记录、以及其创建者、当前拥有者。

contract Election{
    string public constant name = "溯源DAPP";
    string public constant symbol = "DNF767";
    //产品数目
    uint256 public _capacity = 0;
    //管理员地址
    address private _founder;

    //权限状态位、用户创建产品、用户持有产品、产品交易记录等数据
    mapping (address => uint8) public _authorization;
    mapping (uint256 => string) public _cargoesName;
    mapping (address => mapping (uint256 => uint256)) public _cargoes;
    mapping (address => uint256) public _cargoesCount;
    mapping (address => mapping (uint256 => uint256)) public _holdCargoes;
    mapping (uint256 => uint256) public _holdCargoIndex;
    mapping (address => uint256) public _holdCargoesCount;
    mapping (uint256 => mapping (uint256 => Log)) public _logs;
    mapping (uint256 => uint256) public _transferTimes;

    //相应事件
    event NewCargo(address indexed _creater, uint256 _cargoID);
    event Transfer(uint256 indexed _cargoID, address indexed _from, address _to);
   

    //日志结构体
    struct Log {
        uint256 time;
        address holder;
    }
    
   mapping (uint => productnew) public productnews;//产品索引

    //新建产品结构体
    struct productnew{
         uint id;
         
         uint num;//产品序号-溯源码
         uint createTime;//出厂时间
         address  nowhold;//当前持有者地址
         address  createMan;//创建者地址
         string cargoName;//名称

       
         uint tranTIME;//转移时间
         address  befhold;//前一个持有者地址
          
    }
    //构造函数，合约创建时候调用
    constructor () public {
        _founder = msg.sender;
        _authorization[msg.sender] = 3;
    }

    //总产品数量
    function capacity () public view returns (uint256) { return _capacity; }
   //当前地址产品数
    function capacityOf (address _owner) public view returns (uint256) { return _cargoesCount[_owner]; }
    //根据产品ID查询产品名称
    function cargoNameOf (uint256 _cargoID) public view returns (string memory) { return _cargoesName[_cargoID]; }
    //查询该账户地址的权限
    function permissionOf (address _user) public view returns (uint8) { return _authorization[_user]; }
    //根据产品ID查询产品流转次数
    function transferTimesOf (uint256 _cargoID) public view returns (uint256) {
        return _transferTimes[_cargoID];
    }

    //////////////////////////////////////////////////////
    //当前产品ID的持有者地址
     //////////////////////////////////////////////////////
    function holderOf (uint256 _cargoID) public view returns (address) {
        return _logs[_cargoID][_transferTimes[_cargoID]].holder;
    }

    //////////////////////////////////////////////////////
    //该产品ID转移记录 返回持有时间与账户地址  
    //////////////////////////////////////////////////////uint256[] memory times,
    function tracesOf (uint256 _cargoID) public view returns ( address[] memory holders) {
        uint256 transferTime = _transferTimes[_cargoID];
        holders = new address[](transferTime + 1);
        uint256[] memory times;
        times = new uint256[](transferTime + 1);
        for (uint256 i = 0; i <= transferTime; i++) {
            Log memory log = _logs[_cargoID][i];
            holders[i] = log.holder;
            times[i] = log.time;
        }
        return holders;
    }

    //////////////////////////////////////////////////////
    //当前该地址创建的产品（包括以及转移走的）
    //////////////////////////////////////////////////////
    function allCreated () public view returns (uint256[] memory cargoes) {
         address _creater= msg.sender;
        uint256 count = _cargoesCount[_creater];
        cargoes = new uint256[](count);
        for (uint256 i = 0; i < count; i++) {
            cargoes[i] = _cargoes[_creater][i];
        }
    }


    //////////////////////////////////////////////////////
    //当前地址持有的产品
    //////////////////////////////////////////////////////
    function allHolding (address _owner) public view returns (uint256[] memory cargoes) {
        uint256 count = _holdCargoesCount[_owner];
        cargoes = new uint256[](count);
        for (uint256 i = 0; i < count; i++) {
            cargoes[i] = _holdCargoes[_owner][i + 1];
        }
    }



    //////////////////////////////////////////////////////
    //新货物创建-输入货物名字，返回产品创建好的ID
    //////////////////////////////////////////////////////
    function createNewCargo (string memory _cargoName) public returns (uint256 cargoID) {
        uint8 authorization = _authorization[msg.sender];
        require(authorization > 1, "未授权");
        uint256 count = _cargoesCount[msg.sender];
     
        //新建生成一个唯一产品ID
        cargoID = uint(keccak256(abi.encodePacked(msg.sender, count, _capacity)))%10000000000;
   
         _cargoes[msg.sender][count] = cargoID;
        //产品名称存储
        _cargoesName[cargoID] = _cargoName;
        //返回创建产品日志
        _logs[cargoID][0] = Log({
            time: block.timestamp,
            holder: msg.sender
        });
        _addToHolder(msg.sender, cargoID);
        emit NewCargo(msg.sender, cargoID);
        _cargoesCount[msg.sender]++;

       _capacity++;
          // 创建产品，并赋予索引
        productnews[_capacity] = productnew( _capacity,cargoID, now, msg.sender, msg.sender, _cargoName, now, msg.sender);      
    }



    //////////////////////////////////////////////////////
    //设置权限 地址，状态真为授权
    //////////////////////////////////////////////////////
    function setPermission (address _address, uint8 _state) public {
           //判断超级管理员
           uint8 authorization = _authorization[msg.sender];
           require(authorization > 2, "未授权");
            _authorization[_address] = _state;//0无权限-消费者；1有转发权限-物流/中转商；2创建商-有创建权限
     }



    ////////////////////////////////////////////////////////////////
    ////////转移产品。产品持有者调用-物流转移经销商才可以转移////////////
    ////////////////////////////////////////////////////////////////
    function transfer (uint256 _cargoID, address _to) public returns (bool success) {
        uint8 authorization = _authorization[msg.sender];
        require(authorization > 0, "非合法经销商！");
        uint256 transferTime = _transferTimes[_cargoID];
        address holder = _logs[_cargoID][transferTime].holder;
        //  产品拥有者不为零地址 
        require(holder != address(0), "无效地址！");
        //  产品拥有者必须是交易的发送者，即有该产品当前的转移权，这是最关键的条件
        require(holder == msg.sender, "非当前产品拥有者！");
        //  目标地址不能为交易的发送者
        require(holder != _to, "不能转移给自己！");
        // 目标地址不能为零地址
        require(_to != address(0), "无效收件地址！");

        // 当条件都符合的时候，产品的转移记录会增加：
        _transferTimes[_cargoID]++;
        _logs[_cargoID][transferTime + 1] = Log({
            time: block.timestamp,
            holder: _to
        });
       // 合约内的私有方法，先后移除原拥有者的持有权，并且将其移动给目标用户
        _removeFromHolder(msg.sender, _cargoID);
        _addToHolder(_to, _cargoID);
        //（emit）一个相应的事件（Event），这样可以在区块链上对应的`transactionReceipt`记录中找到相应的事件信息，
        emit Transfer(_cargoID, holder, _to);

        //新建转移产品方法
        uint littleID=0;
        for(uint i=0;i<=_capacity;i++){
            productnew storage productTemp = productnews[i];
            if(productTemp.num== _cargoID){
                 littleID=i;
            }
        }
        require(littleID!=0);
         productnew storage productnewa = productnews[littleID];
         productnewa.befhold= productnewa.nowhold ;//前一个持有者地址
         
         productnewa.nowhold=_to;
         productnewa.tranTIME=now;
        
        
        return true;
    }


    //////////////////////////////////////////////////////
    //合约内私有方法，将目标_cargoID产品--》从原持有ID地址移除
    //////////////////////////////////////////////////////
    function _removeFromHolder (address _oriHolder, uint256 _cargoID) private {
        uint256 count = _holdCargoesCount[_oriHolder];
        uint256 index = _holdCargoIndex[_cargoID];
        _holdCargoes[_oriHolder][index] = _holdCargoes[_oriHolder][count];
        _holdCargoesCount[_oriHolder]--;
    }



    //////////////////////////////////////////////////////
    //合约内私有方法，将目标_cargoID产品--》转移给新地址_newHolder
    //////////////////////////////////////////////////////
    function _addToHolder (address _newHolder, uint256 _cargoID) private {
        uint256 count = _holdCargoesCount[_newHolder];
        _holdCargoIndex[_cargoID] = count + 1;
        _holdCargoes[_newHolder][count + 1] = _cargoID;
        _holdCargoesCount[_newHolder]++;
    }


}
