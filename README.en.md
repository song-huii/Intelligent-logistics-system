# 基于区块链和无人机的智慧物流系统

#### Description
本项目设计实现的YOU快递智慧物流系统,结合物流无人机实现区域内和区域间的快递的智慧配送,结合区块链技术,利用Fabric平台构建数据层,为每一件快递定制专属二维码,轻松实现快递溯源,极大的保证了用户隐私数据和快递的安全性｡而项目中利用蚁群算法和边缘计算技术为项目实现提供了底层支撑,通过蚁群算法对物流无人机的路径进行规划,保证无人机的飞行距离最短,节约成本。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
