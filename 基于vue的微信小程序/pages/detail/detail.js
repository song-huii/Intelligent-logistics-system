var app = getApp();

Page({

  data: {

    express: {}

  },

  onLoad: function (options) {

    this.setData({ express: app.globalData.express });
    this.setData({ status: app.globalData.expressStatus });

  },
})