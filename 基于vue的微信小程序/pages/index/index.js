var app = getApp();

Page({

  data: {
    expNum: {},
    express: {},
    searchInput: '',
    showClear: 'none',
    history: [],
    showNothing: 'block',
    showClearHistory: 'none',
  },

  onLoad: function (options) {
    // 传递对象
    var that = this
    // 获取历史记录
    const historyOld = wx.getStorageSync('history') || []
    // 设置全局变量
    this.setData({ history: historyOld })
    // 判断是否有记录
    if (this.data.history.length == 0) {
      that.setData({
        showNothing: 'block',
        showClearHistory: 'none',
      })
    } else {
      that.setData({
        showNothing: 'none',
        showClearHistory: 'flex',
      })
    }
  },

  // 通过快递单号查询快递详情
  expressSearch: function (expNum) {
    wx.showToast({
      title: '正在查询...',
      icon: 'loading',
      duration: 2000000,
    })
    // 传递对象
    var that = this
    // 发起请求
    wx.request({
      url: 'https://ali-deliver.showapi.com/showapi_expInfo?com=auto&nu=' + expNum,
      data: {},
      header: {
        'Authorization': 'APPCODE eb40edc07d25455496995febb87b007e'
      },

      success: function (res) {
        // 显示加载提示框
        wx.hideToast();
        // 是否返回数据
        if (res.data.showapi_res_body.status == "-1" || res.data.showapi_res_body.status == "0" || res.data.showapi_res_body.status == "1" || res.data.showapi_res_body.ret_code == "-1") {
          // 显示提示框
          wx.showModal({
            title: '查询失败',
            content: '单号不存在或暂无此快递信息！',
            showCancel: false,
          })
        } else {
          // 处理数据
          for (var i = 0; i < res.data.showapi_res_body.data.length; i++) {
            var date = res.data.showapi_res_body.data[i].time.substring(5, 10);
            var time = res.data.showapi_res_body.data[i].time.substring(11, 16);
            res.data.showapi_res_body.data[i].date = date;
            res.data.showapi_res_body.data[i].time = time;
          }
          // 处理状态数据
          switch (res.data.showapi_res_body.status) {
            case 2:
              app.globalData.expressStatus = "运输中";
              break;
            case 3:
              app.globalData.expressStatus = "派送中";
              break;
            case 4:
              app.globalData.expressStatus = "已签收";
              break;
            case 5:
              app.globalData.expressStatus = "用户拒签";
              break;
            case 6:
              app.globalData.expressStatus = "疑难件";
              break;
            case 7:
              app.globalData.expressStatus = "无效单";
              break;
            case 8:
              app.globalData.expressStatus = "超时单";
              break;
            case 9:
              app.globalData.expressStatus = "签收失败";
              break;
            case 10:
              app.globalData.expressStatus = "退回";
              break;
          }
          // 保存数据
          app.globalData.express = res.data.showapi_res_body;
          // 跳转到快递详情界面
          wx.navigateTo({ 
            url: '../detail/detail',
          })
          // 提取全局变量
          var history = wx.getStorageSync('history') || [] 
          // 删除所查单号已存在的记录
          for (var i = 0; i < history.length; i++) {  
            if (history[i].number == res.data.showapi_res_body.mailNo) {
              history.splice(i, 1);
              break;
            }
          }
          // 假如已存在9个记录则删除最后一个记录
          if (that.data.history.length > 9) { 
            history.pop()
          }
          // 将刚刚查询的快递信息插入到历史记录第一位
          history.unshift({ 
            'name': res.data.showapi_res_body.expTextName,
            'number': res.data.showapi_res_body.mailNo,
            'status': app.globalData.expressStatus,
            'detail': res.data.showapi_res_body.data[0].context,
            'time': res.data.showapi_res_body.data[0].time,
            'date': res.data.showapi_res_body.data[0].date,
          })
          // 存入全局变量
          wx.setStorageSync('history', history) 
          // 是否隐藏图片
          if (history !== 0) { 
            that.setData({
              showNothing: 'none',
              showClearHistory: 'flex',
            })
          } else {
            that.setData({
              showNothing: 'block',
              showClearHistory: 'none',
            })
          }

          that.setData({
            history: history,
            searchInput: '',
            showClear: 'none',
          })
        }
      },
      fail: function () {
        wx.hideToast()
        wx.showModal({
          title: '网络超时',
          content: '连接服务器失败,请检查网络设置！',
          showCancel: false,
        })
      }
    })
  },

  // 输入快递单号
  inputExpressSearch: function (e) {
    // 获取输入
    this.setData({ expNum: e.detail.value })
    // 显示一键清空输入按钮
    if (e.detail.value.length > 0) {
      this.setData({
        showClear: 'block',
      })
    } else {
      // 隐藏按钮
      this.setData({
        showClear: 'none',
      })
    }
  },

  // 搜索按钮函数
  btnExpressSearch: function () {
    this.expressSearch(this.data.expNum)
  },

  // 扫描条形码查询
  scanExpressSearch: function () {
    // 调用微信扫码API
    wx.scanCode({
      success: (res) => {
        this.expressSearch(res.result)
      }
    })
  },
  
  // 清空输入框内容
  clearInput: function (e) {
    this.setData({
      searchInput: '',
      showClear: 'none',
    })
  },

  // 清空历史记录
  clearHistory: function () {
    var that = this
    //显示提示框
    wx.showModal({
      title: '提示',
      content: '确定要清除全部查询记录吗？',
      success: function (res) {
        if (res.confirm) {
          // 清空
          wx.removeStorage({
            key: 'history',
            success: function (res) { }
          })

          that.setData({
            history: [],
            showNothing: 'block',
            showClearHistory: 'none',
          })
        }
      }
    })
  },

  // 获取快递详情
  getDetail: function (e) {
    this.expressSearch(e.currentTarget.dataset.number)
  },

  // 分享
  onShareAppMessage: function () {
    return {
      title: '轻快递',
      desc: '简单好用的快递查询工具',
      path: '/pages/index/index'
    }
  }
})