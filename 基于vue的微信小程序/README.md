# 微信小程序-轻快递

## 注意事项
- 本项目中的快递查询API密钥为私人购买，仅供测试！发布作品请勿使用本项目的密钥，谢谢！

## 功能描述
- 一个界面简洁的快递查询小程序。
- 本小程序已在微信中发布，搜索“轻快递”即可找到。

## 程序截图
<img src="https://github.com/iFasWind/WeApp-EasyExpress/blob/master/screenshots/Screenshot_01.png" width="280px" style="display:inline;"> <img src="https://github.com/iFasWind/WeApp-EasyExpress/blob/master/screenshots/Screenshot_02.png" width="280px" style="display:inline;"> <img src="https://github.com/iFasWind/WeApp-EasyExpress/blob/master/screenshots/Screenshot_03.png" width="280px" style="display:inline;">

## 开发环境
- Windows 10 1803
- 微信开发者工具 v1.02.1806120

## 目录结构
- images - 存放项目所需图片资源
- pages - 存放页面相关文件
- screenshots - 存放小程序截图
- utils - 存放utils文件

## 项目作者
- iFasWind

## 联系方式
- ifaswind@qq.com
- ifaswind@gmail.com
