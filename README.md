# 基于区块链和无人机的智慧物流系统

#### 介绍
本项目设计实现的YOU快递智慧物流系统,结合物流无人机实现区域内和区域间的快递的智慧配送,结合区块链技术,利用Fabric平台构建数据层,为每一件快递定制专属二维码,轻松实现快递溯源,极大的保证了用户隐私数据和快递的安全性｡而项目中利用蚁群算法和边缘计算技术为项目实现提供了底层支撑,通过蚁群算法对物流无人机的路径进行规划,保证无人机的飞行距离最短,节约成本。

#### 软件架构
http://47.113.229.131:9080/wms 用户名：test 密码：123456 本地启动的用户名：admin 密码：llg123 android app也已经开源，近期更新会比较频繁 app开源地址 https://gitee.com/song-huii/Intelligent-logistics-system 大家看到了，star一下，谢谢，本团队会持续更新，一直开源！ 欢迎喜欢的朋友一起来优化功能。


#### 安装教程

1，开发环境：
   开发工具：
	IDEA（强烈建议用IDEA，也可以Eclipse）；ANDROID STUDIO
	JDK1.8
	Maven
	Mysql5.6以上（linux 注意设置大小写不敏感）
	运行环境：CENTOS6.5以上或windows server 2008、tomcat7以上，JDK1.8， MYSQL5.7
2，按照mvn方式导入
3，数据库还原：
    安装完数据库执行下 GRANT ALL PRIVILEGES ON *.* TO 'root'@'%'IDENTIFIED BY '你的密码' WITH GRANT OPTION;
                      FLUSH PRIVILEGES;

    步骤 1：还原数据库，2，修改 dbconfig.properties
    3.1 sql导入方式建议 登录MYSQL服务器上用source命令还原
4，IDEA：mvn tomcat7:run   输入用户名和密码：admin llg123
5、主要技术
    开发语言：JAVA。
6、技术架构

#### 使用说明
1、适用范围：第三方物流仓储企业，自营仓储等。
2、技术特点：基于JAVA的WEB后台，基于ANDROID开发的PDA系统。
3、功能特点：涵盖订单管理系统（OMS），仓储管理系统（WMS），计费管理系统（BMS），现场作业系统（RF），第三方接口模块
4、接口支持：已经对接：SAP ECC，SAP HANA 数据库，用友U8，百胜E3，UAS。
5、对接自主研发ERP管理系统（暂未开源）
6、增加进销存模块
7、增加BOM

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
